import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/app/home/component.vue'
import App from './views/app/component.vue'
import Login from './views/auth/login/component.vue'
import Signup from './views/auth/signup/component.vue'
import Settings from './views/app/settings/component.vue'
import Landing from './views/app/landing/component.vue'

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,

    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/landing',
      name: 'landing',
      component: Landing
    },
    {
      path: '/app',
      name: 'app',
      component: App,
      children: [
        {
          path: 'landing',
          name: 'landing',
          component: Landing
        },
        {
          path: 'settings',
          name: 'settings',
          component: Settings
        }
      ]
    }
  ]
})
